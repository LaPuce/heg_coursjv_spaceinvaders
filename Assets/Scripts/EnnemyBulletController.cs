﻿using UnityEngine;

public class EnnemyBulletController : MonoBehaviour
{
    public float Speed = 4f;

    private void Update()
    {
        Move();
    }

    /// <summary>
    ///  Moves the bullet downward
    /// </summary>
    private void Move()
    {
        transform.position = new Vector3(transform.position.x,
            transform.position.y + Speed * Time.deltaTime* -1,
            transform.position.z);
    }

    private void OnTriggerEnter(Collider other)
    {
        // si le projectile touche le joueur
        if (other.tag == "Player")
        {
            ScoreManager.Victory = false;

            // load Menu scene
            LevelLoader.Instance.LoadScene("Menu");
            
        }
        // si le projectile touche un mur
        if (other.tag == "wall")
        {
            // détruire le projectile
            Destroy(gameObject);
        }
    }
}