﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnnemyController : MonoBehaviour
{
    public float Speed = 1f;

    private void Update()
    {
        // move ennemy
        Move();
    }

    private void Move()
    {
        transform.position = new Vector3(
            transform.position.x + Speed * Time.deltaTime,
            transform.position.y,
            transform.position.z);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "wall") {
            // change direction
            Speed *= -1;
        }
    }
}