﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class BulletManager : MonoBehaviour
{
    public GameObject BulletPrefab;
    public List<GameObject> bullets;
    public int BulletCount = 10;

    private void Awake()
    {
        bullets = new List<GameObject>();
        for (int i = 0; i < BulletCount; i++)
        {
            bullets.Add(Instantiate(BulletPrefab));
        }
        for (int i = 0; i < bullets.Count; i++)
        {
            bullets[i].SetActive(false);
        }
    }

    internal void GetBullet(Vector3 _playerPosition)
    {
        for (int i = 0; i < bullets.Count; i++)
        {
            // on regarde s'il y a une bullet de libre (pas active)
            if (!bullets[i].activeSelf) {
                bullets[i].transform.position = _playerPosition;
                bullets[i].SetActive(true);
                return;
            }
        }
    }
}
