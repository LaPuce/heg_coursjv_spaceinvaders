﻿using UnityEngine;

public class EnnemyManager : MonoBehaviour
{
    public static int EnnemyCount;

    public static int ennemyDestroyed;

    void Start()
    {
        // init ennemy destroyed
        ennemyDestroyed = 0;

        // init score
        ScoreManager.Victory = false;

        // count all ennemies
        EnnemyCount = FindObjectsOfType<EnnemyController>().Length;
    }

    public static void EnnemyDestroyed()
    {
        ennemyDestroyed++;
        if (ennemyDestroyed >= EnnemyCount) {
            ScoreManager.Victory = true;
            LevelLoader.Instance.LoadScene("Menu");
        }
    }
}