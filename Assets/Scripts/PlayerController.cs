﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public BulletManager bulletManager;
    public float Speed = 1f;

    private bool touchWallLeft = false;
    private bool touchWallRight = false;
    private AudioSource audioSource;

    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            // tirer un projectile
            bulletManager.GetBullet(transform.position);

            // jouer le son de tir
            audioSource.Play();
        }


        if (Input.GetKey(KeyCode.LeftArrow) && !touchWallLeft) {
            // move player ship to the left
            transform.position = new Vector3(
                transform.position.x - Speed * Time.deltaTime,
                transform.position.y,
                transform.position.z);
        }
        else if (Input.GetKey(KeyCode.RightArrow) && !touchWallRight)
        {
            // move player ship to the right
            transform.position = new Vector3(transform.position.x + Speed * Time.deltaTime, transform.position.y, transform.position.z);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.name == "WallLeft")
            touchWallLeft = true;

        else if (other.name == "WallRight")
            touchWallRight = true;
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.name == "WallLeft")
            touchWallLeft = false;

        else if (other.name == "WallRight")
            touchWallRight = false;
    }
}