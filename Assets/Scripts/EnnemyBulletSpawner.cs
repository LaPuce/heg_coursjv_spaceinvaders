﻿using UnityEngine;

public class EnnemyBulletSpawner : MonoBehaviour
{
    public GameObject bulletPrefab;
    private float lastSpawnTime;
    private float timeout;

    private void Start()
    {
        timeout = Random.Range(0f, 3f);
        lastSpawnTime = Time.time;
    }

    void Update()
    {
        if (Time.time - lastSpawnTime > timeout) {
            Instantiate(bulletPrefab, transform.position, transform.rotation);
            timeout = Random.Range(0f, 3f);
            lastSpawnTime = Time.time;
        }
    }
}