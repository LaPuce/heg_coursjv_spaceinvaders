﻿using TMPro;
using UnityEngine;

public class CanvasScore : MonoBehaviour
{
    public TextMeshProUGUI txt;

    void Start()
    {
        if (ScoreManager.Victory)
            txt.text = "Victoire !";
        else
            txt.text = "Défaite !";
    }

    public void LoadScene(string _name) {
        LevelLoader.Instance.LoadScene(_name);
    }
}