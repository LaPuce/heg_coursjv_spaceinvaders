﻿using UnityEngine;

namespace Controls
{

    public class BulletController : MonoBehaviour
    {
        public float Speed = 4f;

        private void Update()
        {
            Move();
        }

        /// <summary>
        ///  Moves the bullet to the top
        /// </summary>
        private void Move()
        {
            transform.position = new Vector3(transform.position.x,
                transform.position.y + Speed * Time.deltaTime,
                transform.position.z);
        }

        private void OnTriggerEnter(Collider other)
        {
            // si le projectile touche un mur
            if (other.tag == "wall")
            {
                // désactiver le projectile
                gameObject.SetActive(false);
            }

            // si le projectile touche un ennemi
            if (other.tag == "Ennemy")
            {
                EnnemyManager.EnnemyDestroyed();

                // détruire ennemi
                Destroy(other.gameObject);
            }
        }
    }
}