﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextureOffset : MonoBehaviour
{
    private Material mat;

    public Vector2 TextureSpeed;

    void Start()
    {
        mat = GetComponent<Renderer>().material;
    }

    void Update()
    {
        mat.SetTextureOffset("_MainTex",Time.time*TextureSpeed);
    }
}